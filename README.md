## Chat service based on SSE

This chat server uses "go-sse" forked lib (fork was needed to be able to disconnect clients). 

It has two methods: POST and GET and only one endpoint: /infocenter/{topic} (topic is name of the chanel)

To subscribe to the chanel simple request GET.

For posting to the chanel, send msg with POST.