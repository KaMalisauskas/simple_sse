package main

import (
	"net/http"
)

func postMessage(res http.ResponseWriter, req *http.Request) {
	var msg string

	if len(req.Header["Content-Type"]) == 0 || req.Header["Content-Type"][0] != "application/x-www-form-urlencoded" {
		res.Write([]byte("Content type should be application/x-www-form-urlencoded"))

		return
	}

	req.ParseForm()

	for key := range req.Form {
		msg = key
		break
	}

	s.SendToChannel(msg, req.URL.Path, "msg")

	res.WriteHeader(204)
	res.Write([]byte(""))

}
