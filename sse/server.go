package sse

import (
	"log"
	"net/http"
	"sync"
	"time"
)

// Server sse server struct
type Server struct {
	id       int
	t        time.Duration
	channels map[string]*Channel
	debug    bool
	Mux      sync.Mutex
}

// NewServer creates new server
func NewServer(timeOut time.Duration, debug bool) *Server {
	s := &Server{
		id:       0,
		t:        timeOut * time.Second,
		debug:    debug,
		channels: make(map[string]*Channel),
	}

	return s
}

// Serve serves http client with sse stream
func (s *Server) Serve(w http.ResponseWriter, r *http.Request) {
	h := w.Header()

	h.Set("Content-Type", "text/event-stream")
	h.Set("Cache-Control", "no-cache")
	h.Set("Connection", "keep-alive")

	channelName := r.URL.Path

	c, err := newCLient(channelName, &w)

	if err {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.(http.Flusher).Flush()

	s.addClient(c)

	s.connectionTimeout(c, &w)
}

// SendToChannel send message to wanted channel
func (s *Server) SendToChannel(msg, cName, event string) {
	s.Mux.Lock()
	defer s.Mux.Unlock()

	ch, exists := s.getChannel(&cName)

	if !exists {
		s.log("Channel does not exist, but tried to send: %s", cName)
		return
	}

	s.id++

	ch.SendMessage(NewMessage(s.id, msg, event))

}

//ShutDown closes sse server
func (s *Server) ShutDown() {
	for name := range s.channels {
		if ch, exists := s.channels[name]; exists {
			delete(s.channels, name)
			ch.Close()
		}
	}
	log.Println("server stopped")
}

func (s *Server) connectionTimeout(c *Client, w *http.ResponseWriter) {

	notify := (*w).(http.CloseNotifier).CloseNotify()

	select {
	case <-notify:
		s.Mux.Lock()
		defer s.Mux.Unlock()

		if ch, exists := s.getChannel(&c.channel); exists {
			ch.removeClient(c)
			s.log("client disconnected from channel '%s'.", ch.name)
			s.deleteChannelIfCan(ch)
		}

		return
	case <-time.After(s.t):
		s.Mux.Lock()
		defer s.Mux.Unlock()

		cName := c.Channel()
		ch, exists := s.getChannel(&cName)

		if !exists {
			s.log("No chanel %s", cName)
			return
		}

		s.log("client disconnected from channel '%s'.", ch.name)

		c.SendMessage(NewMessage(0, s.t.String(), "timeout"))
		ch.removeClient(c)
		s.deleteChannelIfCan(ch)

		return
	}
}

func (s *Server) deleteChannelIfCan(c *Channel) {
	s.log("checking if channel '%s' has clients.", c.name)
	if c.ClientCount() == 0 {
		delete(s.channels, c.name)
		c.Close()
		s.log("channel '%s' has no clients.", c.name)
	}
}

func (s *Server) getChannel(cName *string) (*Channel, bool) {
	ch, ok := s.channels[*cName]
	return ch, ok
}

func (s *Server) addClient(c *Client) {
	s.Mux.Lock()
	defer s.Mux.Unlock()

	ch, exists := s.channels[c.channel]

	if !exists {
		ch = newChannel(c.channel)
		s.channels[ch.name] = ch
	}

	s.log("Client is being added to channel %s", ch.name)
	ch.addClient(c)
}

func (s *Server) log(msg string, data interface{}) {
	if s.debug {
		log.Printf(msg, data)
	}
}
