package sse

import (
	"sync"
)

// Channel struct
type Channel struct {
	name    string
	clients map[*Client]bool
	Mux     sync.Mutex
}

// newChannel creates new channel
func newChannel(name string) *Channel {
	return &Channel{
		name:    name,
		clients: make(map[*Client]bool),
	}
}

// SendMessage sends message to all clients in the channel
func (c *Channel) SendMessage(msg string) {
	c.Mux.Lock()
	defer c.Mux.Unlock()

	for c, open := range c.clients {
		if open {
			c.SendMessage(msg)
		}
	}
}

// ClientCount returns client amount in the channel
func (c *Channel) ClientCount() int {
	return len(c.clients)
}

// Close disconnect all clients.
func (c *Channel) Close() {
	c.Mux.Lock()
	defer c.Mux.Unlock()

	for client := range c.clients {
		c.removeClient(client)
	}
}

func (c *Channel) addClient(client *Client) {
	c.Mux.Lock()
	c.clients[client] = true
	c.Mux.Unlock()
}

func (c *Channel) removeClient(client *Client) {
	c.Mux.Lock()
	defer c.Mux.Unlock()

	c.clients[client] = false
	delete(c.clients, client)
}
