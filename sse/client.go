package sse

import (
	"fmt"
	"net/http"
	"sync"
)

// Client structure
type Client struct {
	channel string
	w       *http.ResponseWriter
	flusher http.Flusher
	Mux     sync.Mutex
}

// newClient creates new client
func newCLient(channel string, w *http.ResponseWriter) (*Client, bool) {
	flusher, ok := (*w).(http.Flusher)

	if !ok {
		http.Error(*w, "Streaming unsupported.", http.StatusInternalServerError)
		return nil, true
	}

	return &Client{
		w:       w,
		channel: channel,
		flusher: flusher,
	}, false
}

// Channel returns the channel where this client is subscribe to.
func (c *Client) Channel() string {
	return c.channel
}

// SendMessage sents message to the client
func (c *Client) SendMessage(msg string) {
	c.Mux.Lock()
	defer c.Mux.Unlock()

	fmt.Fprintf(*c.w, msg)
	c.flusher.Flush()
}
