package sse

import (
	"bytes"
	"fmt"
	"strings"
)

func NewMessage(id int, data, event string) string {
	var buffer bytes.Buffer

	if id > 0 {
		buffer.WriteString(fmt.Sprintf("id: %d\n", id))
	}

	if len(event) > 0 {
		buffer.WriteString(fmt.Sprintf("event: %s\n", event))
	}

	if len(data) > 0 {
		buffer.WriteString(fmt.Sprintf("data: %s\n", strings.Replace(data, "\n", "\ndata: ", -1)))
	}

	buffer.WriteString("\n")

	return buffer.String()
}
