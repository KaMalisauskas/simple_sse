package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/KaMalisauskas/aardvark_sse/sse"
	"github.com/gorilla/mux"
)

// ConfigJSON config.json struct
type ConfigJSON struct {
	Port    string        `json:"port"`
	MaxTime time.Duration `json:"maxTime"`
}

var config ConfigJSON
var s *sse.Server

func (c *ConfigJSON) read() {
	file, err := os.Open("config.json")

	if err != nil {
		panic(err)
	}

	defer file.Close()

	if err := json.NewDecoder(file).Decode(&c); err != nil {
		panic(err)
	}
}

func init() {
	config.read()
}

func main() {
	PORT := config.Port

	s = sse.NewServer(config.MaxTime, true)
	defer s.ShutDown()

	r := mux.NewRouter()

	r.HandleFunc("/infocenter/{topic}", postMessage).Methods("POST")

	r.HandleFunc("/infocenter/{topic}", s.Serve).Methods("GET")

	fmt.Println("Runing server on port: ", PORT)

	log.Fatal(http.ListenAndServe(":"+PORT, r))

}
